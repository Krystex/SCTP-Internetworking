#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#define MAX_BUFFER 1024
#define MY_PORT_NUM 62324 /* This can be changed to suit the need and should be same in server and client */
#define ADDRESS1 "192.168.100.1"
#define ADDRESS2 "192.168.200.1"

int main(int argc, char* argv[]) {
    int connSock, in, i, ret, flags;
    int counter = 0;
    struct sockaddr_in servaddr1;
    struct sockaddr_in servaddr2;
    struct sctp_status status;
    char buffer[MAX_BUFFER + 1];
    char buffer2[MAX_BUFFER + 1];
    int datalen = 0;
    struct sctp_initmsg initmsg;
    struct sctp_paddrparams heartbeat;
    struct sctp_rtoinfo rtoinfo;
    struct sockaddr_in *paddrs[10];

    /*Get the input from user*/
    printf("Enter data to send: ");
    fgets(buffer, MAX_BUFFER, stdin);
    //sprintf(buffer, "Packet %i", counter++);
    /* Clear the newline or carriage return from the end*/
    buffer[strcspn(buffer, "\r\n")] = 0;
    /* Sample input */
    //strncpy (buffer, "Hello Server", 12);
    //buffer[12] = '\0';
    datalen = strlen(buffer);

    connSock = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP);

    if (connSock == -1) {
        printf("Socket creation failed\n");
        perror("socket()");
        exit(1);
    }

    bzero((void *) &servaddr1, sizeof (servaddr1));
    servaddr1.sin_family = AF_INET;
    servaddr1.sin_port = htons(MY_PORT_NUM);
    servaddr1.sin_addr.s_addr = inet_addr(ADDRESS1);
    
    bzero((void *) &servaddr2, sizeof (servaddr2));
    servaddr2.sin_family = AF_INET;
    servaddr2.sin_port = htons(MY_PORT_NUM);
    servaddr2.sin_addr.s_addr = inet_addr(ADDRESS2);
    
    memset(&initmsg, 0, sizeof(struct sctp_initmsg));
    initmsg.sinit_num_ostreams = 2;
    initmsg.sinit_max_instreams = 2;
    initmsg.sinit_max_attempts = 4;
    if((ret = setsockopt(connSock, SOL_SCTP, SCTP_INITMSG, &initmsg, sizeof(initmsg))) != 0)
        perror("setsockopt");
    
    memset(&heartbeat,  0,   sizeof(struct sctp_paddrparams));
    heartbeat.spp_flags = SPP_HB_ENABLE;
    heartbeat.spp_hbinterval = 5000;
    heartbeat.spp_pathmaxrxt = 1;
    if((ret = setsockopt(connSock, SOL_SCTP, SCTP_PEER_ADDR_PARAMS , &heartbeat, sizeof(heartbeat))) != 0)
        perror("setsockopt");
    
    memset(&rtoinfo,    0, sizeof(struct sctp_rtoinfo));
    rtoinfo.srto_max = 2000;
    if((ret = setsockopt(connSock, SOL_SCTP, SCTP_RTOINFO , &rtoinfo, sizeof(rtoinfo))) != 0)
        perror("setsockopt");


    
    /*Get Peer Addresses*/
    /*
    int addr_count = sctp_getpaddrs(connSock, 0, (struct sockaddr**)paddrs);
    printf("\nPeer addresses: %d\n", addr_count);
    // Print Out Addresses
    for(i = 0; i < addr_count; i++)
        printf("Address %d: %s:%d\n", i +1, inet_ntoa((*paddrs)[i].sin_addr), (*paddrs)[i].sin_port);
    sctp_freepaddrs((struct sockaddr*)*paddrs);
    
    ret = connect(connSock, (struct sockaddr *) &servaddr1, sizeof (servaddr1));

    if (ret == -1) {
        printf("Connection failed\n");
        perror("connect()");
        close(connSock);
        exit(1);
    }
    */
    /*ret = connect(connSock, (struct sockaddr *) &servaddr2, sizeof (servaddr2));
    if (ret == -1) {
        printf("Connection failed\n");
        perror("connect()");
        close(connSock);
        exit(1);
    }*/
    sctp_assoc_t assoc_id = 0;
    
    struct sockaddr_in addrs[2];
    addrs[0] = servaddr1;
    addrs[1] = servaddr2;
    ret = sctp_connectx(connSock, (struct sockaddr*)&addrs, 2, &assoc_id);
    if (ret == -1) {
        printf("Connection failed\n");
        perror("sctp_connectx()");
        close(connSock);
        exit(1);
    }
    printf("OK. Association id is %d\n", assoc_id);
    

    while (1){
        counter++;
        ret = sctp_sendmsg(connSock, (void *) buffer, (size_t) datalen,
            NULL, 0, 0, 0, counter % 2, 0, 0);
        
        if (ret == -1) {
            printf("Error in sctp_sendmsg\n");
            perror("sctp_sendmsg()");
        } else
            printf("Successfully sent %d bytes data to server\n", ret);
        
        if(sctp_recvmsg(connSock, buffer2, sizeof (buffer2),
                (struct sockaddr *) NULL, 0, &sndrcvinfo, &flags) == -1)
            perror("sctp_recvmsg()");
    
        
        sleep(1);
    }
    

    close(connSock);

    return 0;
}
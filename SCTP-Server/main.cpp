#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#define MAX_BUFFER 1024
#define MY_PORT_NUM 62324 /* This can be changed to suit the need and should be same in server and client */

int
main() {
    int listenSock, connSock, ret, in, flags, i;
    struct sockaddr_in servaddr;
    struct sctp_initmsg initmsg;
    struct sctp_event_subscribe events;
    struct sctp_sndrcvinfo sndrcvinfo;
    struct sctp_paddrparams heartbeat;
    struct sctp_rtoinfo rtoinfo;
    char buffer[MAX_BUFFER + 1];
    int counter = 0;

    listenSock = socket(AF_INET, SOCK_SEQPACKET, IPPROTO_SCTP);
    if (listenSock == -1) {
        printf("Failed to create socket\n");
        perror("socket()");
        exit(1);
    }

    bzero((void *) &servaddr, sizeof (servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(MY_PORT_NUM);
    
    memset(&heartbeat,  0, sizeof(struct sctp_paddrparams));
    heartbeat.spp_flags = SPP_HB_ENABLE;
    heartbeat.spp_hbinterval = 5000;
    heartbeat.spp_pathmaxrxt = 1;
    
    memset(&rtoinfo, 0, sizeof(struct sctp_rtoinfo));
    rtoinfo.srto_max = 2000;
    
    if (setsockopt(listenSock, SOL_SCTP, SCTP_PEER_ADDR_PARAMS, &heartbeat, sizeof(heartbeat)) != 0) {
        perror("Heartbeat");
        
    }
    
    if(setsockopt(listenSock, SOL_SCTP, SCTP_RTOINFO , &rtoinfo, sizeof(rtoinfo)) != 0)
        perror("setsockopt");

    ret = bind(listenSock, (struct sockaddr *) &servaddr, sizeof (servaddr));

    if (ret == -1) {
        printf("Bind failed \n");
        perror("bind()");
        close(listenSock);
        exit(1);
    }

    /* Specify that a maximum of 2 streams will be available per socket */
    memset(&initmsg, 0, sizeof (initmsg));
    initmsg.sinit_num_ostreams = 2;
    initmsg.sinit_max_instreams = 2;
    initmsg.sinit_max_attempts = 4;
    ret = setsockopt(listenSock, IPPROTO_SCTP, SCTP_INITMSG,
            &initmsg, sizeof (initmsg));

    if (ret == -1) {
        printf("setsockopt() failed \n");
        perror("setsockopt()");
        close(listenSock);
        exit(1);
    }

    ret = listen(listenSock, 5);
    if (ret == -1) {
        printf("listen() failed \n");
        perror("listen()");
        close(listenSock);
        exit(1);
    }

    while (1) {
        sleep(1);
        char buffer[MAX_BUFFER + 1];
        int len;

        //Clear the buffer
        bzero(buffer, MAX_BUFFER + 1);

        printf("Awaiting a new connection\n");

        /*connSock = accept(listenSock, (struct sockaddr *) NULL, (socklen_t *) NULL);
        if (connSock == -1) {
            printf("accept() failed\n");
            perror("accept()");
            close(connSock);
            continue;
        } else
            printf("New client connected....\n");*/
        while(1) {
            counter++;
            in = sctp_recvmsg(connSock, buffer, sizeof (buffer),
                (struct sockaddr *) NULL, 0, &sndrcvinfo, &flags);

            if (in == -1) {
                printf("Error in sctp_recvmsg\n");
                perror("sctp_recvmsg()");
                close(listenSock);
                continue;
            } else {
                //Add '\0' in case of text data
                buffer[in] = '\0';

                printf(" Length of Data received: %d\n", in);
                printf(" Data : %s\n", (char *) buffer);
            }
            
            if (sctp_sendmsg(connSock, (void *) buffer, (size_t) datalen, NULL, 0, 0, 0, counter % 2, 0, 0) == -1)
                perror("sctp_sendmsg");
                
            sleep(1);
        }
        
        close(connSock);
    }

    return 0;
}